
for i=1:length(data);
% tic;
fs=3051.76;

w = data(i).resp;
% % % % % puffs=data(i).puffs;
unfiltered = w; %unfiltered respiration data
framevector=1:numel(unfiltered);

% Filter with bandpass butterworth filter
[b,a] = butter(1,1/(fs/2),'high');
y = filtfilt(b,a,unfiltered);
% plot(y)
[d,c] = butter(5,5/(fs/2),'low');
filtered = filtfilt(d,c,y);
% figure,plot(filtered);

filteredminus=-filtered;
protophase = co_hilbproto(filteredminus,0,0,0,0);
[phi_phase_of_respiration_frames arg sig] = co_fbtrT(protophase);
timevector=(1:numel(filtered))/fs;
plot(timevector,phi_phase_of_respiration_frames)
title('phase transform of the respiration')
xlabel('time (s)')
ylabel('phase of the respiration')

resetphase=find(phi_phase_of_respiration_frames<=0.04);% returns vector of indices where phase<0.04
jj=1:length(resetphase)-1;
bb(jj+1)=resetphase(jj+1)-resetphase(jj);
dd=find(bb~=1);  % eliminate dubbel indices (where first couple indices are under 0.04)
rr=resetphase(dd)/fs; % returns column vector with the correct zero points for one block, in frames
oo=1:length(rr)-1;
averagedi(oo)=abs(rr(oo+1)-rr(oo)); % time in between each breath
avfreq=1./mean(averagedi); % Find the frequency of respiration
zeropoints(oo)=rr(oo); %array with zero points of every block in each column
ggg=find(zeropoints==0);
    if zeropoints(oo)==0
        zeropoints(oo)=NaN;
    end
   data(i).inhalation=(zeropoints')*fs;
   data(i).inhalation=data(i).inhalation*1000;
   data(i).phi_phase_of_respiration_frames=phi_phase_of_respiration_frames;
   data(i).filtered=filtered;
   data(i).timevector=timevector;
   clearvars -except data i fs
   close all
end

%% To calc ihnalation in seconds
for i=1:length(data);
data(i).inhalation_in_sec=(data(i).inhalation/fs)/1000;
end
%%
for i=1:length(data);
fs=3051.76;
timevector=(1:numel(data(i).filtered))/fs;
figure,
% plot((data(i).Resp*fs),'g' )
hold on
plot(timevector,data(i).filtered*3000,'m' )
plot(timevector,data(i).phi_phase_of_respiration_frames-20)
line([data(i).inhalation_in_sec data(i).inhalation_in_sec]', repmat([30 40],size(data(i).inhalation_in_sec,1),1)','color','k')
% line([data(i).ss data(i).ss]', repmat([10 20],size(data(i).ss ,1),1)','color','b')
% line([data(i).cs data(i).cs ]', repmat([20 30],size(data(i).cs ,1),1)','color','r')
% line([data(i).puffs data(i).puffs]', repmat([-20 30],size(data(i).puffs,1),1)','color','k')
end

%%
for i=1:length(data);
fs=3051.76;
figure,


line([data(i).inhalation_in_sec data(i).inhalation_in_sec]', repmat([10 20],size(data(i).inhalation_in_sec,1),1)','color','k')
% line([data(i).ss data(i).ss]', repmat([10 20],size(data(i).ss ,1),1)','color','b')
% line([data(i).cs data(i).cs ]', repmat([20 30],size(data(i).cs ,1),1)','color','r')
line([data(i).puffs data(i).puffs]', repmat([20 30],size(data(i).puffs,1),1)','color','k')
end
%%
for i=6:length(data);
fs=3051.76;
timevector=(1:numel(data(i).filtered))/fs;
figure,
% plot((data(i).Resp*fs),'g' )
hold on
plot(timevector,data(i).filtered*3000,'m' )
% plot(timevector,data(i).phi_phase_of_respiration_frames-20)
% line([data(i).inhalation_in_sec data(i).inhalation_in_sec]', repmat([30 40],size(data(i).inhalation_in_sec,1),1)','color','k')
% line([data(i).ss data(i).ss]', repmat([10 20],size(data(i).ss ,1),1)','color','b')
% line([data(i).cs data(i).cs ]', repmat([20 30],size(data(i).cs ,1),1)','color','r')
line([data(i).puffs data(i).puffs]', repmat([20 30],size(data(i).puffs,1),1)','color','k')
end
%% TO save big file
% print(gcf, '-dpdf', '171220_example_raw_glur3_3', '-r3000')

 close ALL

%  save('13_mice_with_markers_and_inhalation.mat', '-v7.3') ;
%  
%   save('13_mice_rESULT.mat', '-v7.3') ;
% clearvars -except data i fs
% toc

% plot(timevector*1000,filtered*5000,'m' )
%%
% % % 
% % % for i=1:length (data);
% % % % tic;
% % %  data(i).inhalation=data(i).inhalation*fs;
% % % end
%% To plot and check

% 
% for i=1%:length(data);
% fs=3051.76;
% timevector=(1:numel(data(i).filtered))/fs;
% figure,plot((data(i).Resp*fs),'g' )
% hold on
% plot(timevector*fs,data(i).filtered*5000,'m' )
% plot(timevector*fs,data(i).phi_phase_of_respiration_frames)
% line([data(i).inhalation/1000 data(i).inhalation/1000]', repmat([-100 100],size(data(i).inhalation/1000,1),1)','color','r')
% % line([data(i).cs*1000 data(i).cs*1000 ]', repmat([100 200],size(data(i).cs*1000 ,1),1)','color','g')
% 
% line([data(i).puffs*1000 data(i).puffs*1000 ]', repmat([0 200],size(data(i).puffs*1000 ,1),1)','color','k')
% end

%%
for i=1%:length(data);
fs=3051.76;
timevector=(1:numel(data(i).filtered))/fs;
figure,
% plot((data(i).Resp*fs),'g' )
hold on
plot(timevector,data(i).filtered*3000,'m' )
plot(timevector,data(i).phi_phase_of_respiration_frames-20)
line([data(i).inhalation_in_sec data(i).inhalation_in_sec]', repmat([30 40],size(data(i).inhalation_in_sec,1),1)','color','k')
% line([data(i).ss data(i).ss]', repmat([10 20],size(data(i).ss ,1),1)','color','b')
% line([data(i).cs data(i).cs ]', repmat([20 30],size(data(i).cs ,1),1)','color','r')
line([data(i).puffs data(i).puffs]', repmat([20 30],size(data(i).puffs,1),1)','color','k')
end

close all
